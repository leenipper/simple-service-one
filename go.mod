module gitlab.com/leenipper/simple-service-one

go 1.12

require (
	github.com/golang/protobuf v1.3.2
	github.com/micro/go-micro v1.7.0
)
